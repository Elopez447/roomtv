package com.lopez.eugenio.roomtv.Model;

import com.google.gson.annotations.SerializedName;

public class POSTours {

    @SerializedName("Availability")
    private String mAvailability;
    @SerializedName("Duration")
    private String mDuration;
    @SerializedName("ID")
    private Long mID;
    @SerializedName("Name")
    private String mName;
    @SerializedName("ValidFrom")
    private String mValidFrom;
    @SerializedName("ValidTo")
    private String mValidTo;
    @SerializedName("Validity")
    private String mValidity;
    @SerializedName("DeparturePoint")
    private String departurePoint;
    @SerializedName("Disclaimer")
    private String mDisclaimer;

    public String getmAvailability() {
        return mAvailability;
    }

    public void setmAvailability(String mAvailability) {
        this.mAvailability = mAvailability;
    }

    public String getmDuration() {
        return mDuration;
    }

    public void setmDuration(String mDuration) {
        this.mDuration = mDuration;
    }

    public Long getmID() {
        return mID;
    }

    public void setmID(Long mID) {
        this.mID = mID;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmValidFrom() {
        return mValidFrom;
    }

    public void setmValidFrom(String mValidFrom) {
        this.mValidFrom = mValidFrom;
    }

    public String getmValidTo() {
        return mValidTo;
    }

    public void setmValidTo(String mValidTo) {
        this.mValidTo = mValidTo;
    }

    public String getmValidity() {
        return mValidity;
    }

    public void setmValidity(String mValidity) {
        this.mValidity = mValidity;
    }

    public String getDeparturePoint() {
        return departurePoint;
    }

    public void setDeparturePoint(String departurePoint) {
        this.departurePoint = departurePoint;
    }

    public String getmDisclaimer() {
        return mDisclaimer;
    }

    public void setmDisclaimer(String mDisclaimer) {
        this.mDisclaimer = mDisclaimer;
    }
}
