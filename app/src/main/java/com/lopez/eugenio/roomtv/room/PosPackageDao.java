package com.lopez.eugenio.roomtv.Room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.lopez.eugenio.roomtv.Model.POSPackages;

import java.util.List;

import io.reactivex.Flowable;


@Dao
public interface PosPackageDao {

    @Query("SELECT * FROM pospackages")
    Flowable<List<POSPackages>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<POSPackages> packages);

}
