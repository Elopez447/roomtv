package com.lopez.eugenio.roomtv.network;

import com.lopez.eugenio.roomtv.Model.POSPackages;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface TopviewService {

    @GET("packages")
    Call<List<POSPackages>> getPackages();

}
