package com.lopez.eugenio.roomtv.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "pospackages")
public class POSPackages {
    @Expose
    @SerializedName("adultPrice")
    @ColumnInfo(name = "adultprice")
    private Long adultPrice;
    @Expose
    @SerializedName("category")
    @ColumnInfo(name = "category")
    private String category;
    @Expose
    @SerializedName("id")
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;
    @Expose
    @SerializedName("kidsPrice")
    @ColumnInfo(name = "kidsprice")
    private Long kidsPrice;
    @Expose
    @SerializedName("name")
    @ColumnInfo(name = "name")
    private String name;
    @Expose
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private Boolean status;
    @Expose
    @SerializedName("tourCodes")
    @ColumnInfo(name = "tour_codes")
    private String tourCodes;
    @Expose
    @SerializedName("validity")
    @ColumnInfo(name = "validity")
    private String validity;
    @Expose
    @SerializedName("Description")
    @ColumnInfo(name = "description")
    private String description;



    public POSPackages(@NonNull Long adultPrice, String category, Long id, Long kidsPrice, String name,
                       Boolean status, String tourCodes, String validity, String description){
        this.adultPrice = adultPrice;
        this.category = category;
        this.id = id;
        this.kidsPrice = kidsPrice;
        this.name = name;
        this.status = status;
        this.tourCodes = tourCodes;
        this.validity = validity;
        this.description = description;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKidsPrice() {
        return kidsPrice;
    }

    public void setKidsPrice(Long kidsPrice) {
        this.kidsPrice = kidsPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getAdultPrice() {
        return adultPrice;
    }

    public void setAdultPrice(Long adultPrice) {
        this.adultPrice = adultPrice;
    }


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getTourCodes() {
        return tourCodes;
    }

    public void setTourCodes(String tourCodes) {
        this.tourCodes = tourCodes;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


//    private List<POSTours> tours;


}
