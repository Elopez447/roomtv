package com.lopez.eugenio.roomtv

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lopez.eugenio.roomtv.viewmodel.PackagesFragmentsViewModel


class PackagesFragments : Fragment() {

    companion object {
        fun newInstance() = PackagesFragments()
    }

    private lateinit var viewModel: PackagesFragmentsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.packages_fragments_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PackagesFragmentsViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
