package com.lopez.eugenio.roomtv.network;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.util.Log;
import com.lopez.eugenio.roomtv.Model.POSPackages;
import com.lopez.eugenio.roomtv.Room.AppDatabase;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiFactory implements Callback<List<POSPackages>> {
    private String TAG = "APIFACTORY: ";
    private String DB_TAG = "ROOM DATABASE: ";

    private Context mContext, appContext;
    private Retrofit retrofit = null;
    private TopviewService service;
    private AppDatabase db;
    List<POSPackages> list;

    public ApiFactory(Context context, Context appContext) {
        mContext = context;
        service = getClient().create(TopviewService.class);
        db = Room.databaseBuilder(appContext,
                AppDatabase.class, "packages_database").build();
    }

    private Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://www.topviewapi.com/api/")
                    .build();
        }
        return retrofit;
    }

    public Completable getPackages() {
        Callback<List<POSPackages>> temp = this;
        return Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                Call<List<POSPackages>> call = service.getPackages();
                call.enqueue(temp);
            }
        });

    }

    @Override
    public void onResponse(Call<List<POSPackages>> call, final Response<List<POSPackages>> response) {

//        for (POSPackages pos: response.body()) {
//            Log.d(TAG, pos.getName());
//        }
        Log.d(TAG, response.toString());
        cachePackages(response.body())
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe( () ->{
                        Log.d("API", "success: saved in room");
                }, throwable -> {
                        Log.d("API", "issue:"+throwable);
                });



    }

    public Completable refreshData(){
        return getPackages();
    }

   public Flowable<List<POSPackages>> getLocalPackages(){

        return db.packageDao().getAll();
    }


    private Completable cachePackages(final List<POSPackages> packages) {
//        Thread thread = new Thread() {
//            @Override
//            public void run() {
//                for (POSPackages packages1 : packages) {
//                    db.packageDao().insert(packages1);
//                }
//            }
//        };
//        thread.start();

      return  Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.packageDao().insert(packages);
            }
        });
    }

    @Override
    public void onFailure(Call<List<POSPackages>> call, Throwable t) {
        Log.d(TAG, t.toString());
    }
}
