package com.lopez.eugenio.roomtv.Room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.lopez.eugenio.roomtv.Model.POSPackages;

@Database(entities = {POSPackages.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PosPackageDao packageDao();
}
