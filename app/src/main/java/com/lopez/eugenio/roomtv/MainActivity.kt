package com.lopez.eugenio.roomtv

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.lopez.eugenio.roomtv.network.ApiFactory
import io.reactivex.disposables.CompositeDisposable

class MainActivity : AppCompatActivity() {
    /* Cache response from topview/packages & tours within a roomDatabase <- this happens on viewmodel level, not here
     * Display data through recyclerView <-
      * */

  // lateinit var viewModel:MainAcitivityViewModel
val disposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        viewModel = MainAcitivityViewModel()
//        viewModel.getCacheData()
        val apiFactory = ApiFactory(this, applicationContext)
        apiFactory.getLocalPackages()
                .subscribe({
                    Log.d("MAINACTIVIET", "success:"+it)
                },{
                    Log.d("MAINACTIVIET", "issue:"+it)
                })
    }

}
